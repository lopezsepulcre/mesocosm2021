## GUPPY MESOCOSM EXPERIMENTS
### SUMMER 2021

---

This package serves as a repository of protocols, data, R functions, and analyses for the guppy mesocosm experiment performed in the Summer of 2021 at Tyson Research Center, led by Yusan Yang, and Swanne Gordon.

### Background

The experiment is aimed at looking at the ecosystem effects of:

1. Sexual selection, sexual conflict, and mating strategies in Trinidadian guppies *Poecilia reticulata*.

2. Fear, when guppies are exposed tocues from their natural predator *Chrenicichla alta*.

For aim 1, female fish were placed with males of either their local predation regime, or the complementary. There were five treatments:

- No fish in the channel
- 4 HP males + 4 HP females
- 4 LP males + 4 LP females
- 4 LP males + 4 HP females
- 4 HP males + 4 LP females

This part of the experiment was replicated using HP and LP guppies from two drainages:

1. Aripo drainage: Natural HP and LP populations, estimated to diverged ~1 million years
2. Guanapo drainage: Natural HP population, and LP population at Lower Lalaja introduced in 2008

We ran the two replicates seperately in consecutive rounds in the Summer of 2021. The Aripo round was conducted from 8/9/21 to 9/6/21, and the Gauanpo round was conducted from 6/15/21 to 7/13/21.


For aim 2. There were two additional treatments:

- 8 HP males and females + predator cues
- 8 LP males and females + predator cues

Due to mesocosm number limitation, the additional treatments in aim 2 was not replicated; we only use fish from the Guanapo drainage. The additional treatments were run in both rounds (6/15/21 - 7/13/21 and 8/9/21 - 9/6/21) 



All experiments was performed on F2+ fish, which means that, for the first time, we will be looking at the genetic component of guppy ecosystem effects.


### Page Organization

This page acts as a shared repository of the project materials aimed at reproducibility and consistency. The package files also contain the raw data. In this page, you will find tabs and menus for:

- **Protocols**. All the protocols for the different variables measured throughout the experiment.

- **Data manipulation**. Tutorials detailing how the raw data was transformed into useable derived data and merged masterfiles.

- **Tutorials**. More detailed descriptions on some of the more complex data processing and calculations, such as chemical analysis, the estimation of stream metabolism, or fitting of algal growth curve.

- **Analysis**. Each article describes a specific higher level analysis aimed at answering specific questions.

### Package Usage

If you are a member of the project but that just wants to see how the analyses were made or reports of the results, you will find those under the Articles tab. If you want to use the package or need to contribute data, documentation, or code you must clone this package and install it in your computer.

This project is version controlled via `git` hosted in [GitLab](https://gitlab.com/) to enable sharing and synchronization among collaborators.

In order to have a copy of this package in your computer, you must:

- Have access rights to the GitLab project repository at https://gitlab.com/lopezsepulcre/mesocosm2021

- Have `git` installed and set up in your computer. If you don't, go to  https://git-scm.com to download it.

- If you never used git collaboratively, you will probably have to create an SSH key as described [here](http://happygitwithr.com/ssh-keys.html).


To clone the full project repository, type in Terminal:

```
git clone git@gitlab.com:lopezsepulcre/mesocosm2021.git
```
 
To contribute, you can then open the package with RStudio by clicking on the `mesocosm2021.Rproj` file. Remember to branch, commit and request merges with your code.

To use the package, simply type in RStudio console:

```
library(mesocosm2021)
```

This webpage address is:
https://lopezsepulcre.gitlab.io/mesocosm2021/index.html

