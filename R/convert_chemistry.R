#' Convert from NH3 to NH4
#'
#' Convert measured ammonia-N concentration (mg/L) into ammonium-N given a temperature in Celsius and a pH.
#'
#' @param ammonium Concentration of ammonium measured
#' @param ph pH of the water
#' @param temp Temperature of the water in degrees Celsius
#'
#' @export
#'
NH3toNH4 <- function(ammonium, pH, temp){

  pKa <- 0.09018 + (2727.92/(temp + 273))

  p <- 1/(10^(pKa-ph) +1)

  NH4 <- ((1-p)/p) * ammonium
}
