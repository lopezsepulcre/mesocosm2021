---
title: "Benthic Organic Matter"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

### Overview

Benthic organic matter is sampled by disturbing a known area delimited by a PVC pipe **on day 28**, and sorted in sieves to differentiate between coarse (CBOM) and fine (FBOM) organic matter.

### Material
- 4in PVC pipe
- Plastic cups
- Turkey baster
- Whirl-paks ($1l$ or $500ml$)
- Sorting sieve set ($64\mu m$, $250\mu m$, and $1mm$)
- Squirt bottle
- Ethanol $95\%$
- Rose bengal
- Stereo microscope
- Weighting tins

### Preparation

Dry and pre-weight $32\times 3$ weighting tins and write the weight on the bottom with a sharpie.

### Protocol

**Sampling**

1. Place the PVC pipe over a section of the benthos and dig it in well until it hits the bottom of the channel
2. Stir well the gravel and water, rubbing the gravel with the fingers to detach organic matter and invertebrates
3. Empty the pipe of water (and suspended BOM), using the cups and baster, onto the stack of sieves.
4. Using the squirt bottle, concentrate the contents and put them into separate whirl-pak's. Label them CBOM, PBOM, and FBOM from larger to smaller
5. Add ethanol for preservation to the whirl-paks
6. Ad rose bengal to stain invertebrates.

**Processing**

1. Sort out the stained invertebrates from the samples and store them in scintillation vials in ethanol. Do so under the microscope.
2. Place the BOM portion left on a pre-weight weithting tin and dry in the oven for $24h$ at $50^\circ C$
3. Measure the final weight
4. Ash the tin at $500^\circ C$ for $2h$
5. Measure the final weight

6. Invertebrates will need to be identified and measured. We will discuss this in the Invertebrate section.

### Analysis 
