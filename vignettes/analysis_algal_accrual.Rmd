---
title: "Algal Dynamics"
author: "Andrés López-Sepulcre"
date: "11/19/2021"
output: html_document
---

```{r setup, include=FALSE}
library(mesocosm2021)
library(knitr)
  opts_chunk$set(echo = TRUE)
library(tidyverse)
library(lubridate)
library(nlme)
load(here::here('data/data_tiles.Rda'))
data_tiles
```

Analyze only Aripo drainage

```{r}
data_aripo <- data_tiles %>%
  filter(Round== "2" & cichlid == "N")

data_pred <- data_tiles %>%
  filter(cichlid == "Y")

```

### Data Visualization

Let's look at the data of algal density changes by mesocosm, round, and treatment. For now we will exclude predators.

```{r}
plot_indtile <- data_aripo %>%
  group_by(Round, Block, treatment, Day) %>%
  ggplot(aes(y = Chl, x = Day, group = Mesocosm)) +
    geom_jitter(aes(col = treatment)) +
    facet_grid(Round ~ Block)

plot_indtile

  
plot_bymeso <- data_aripo %>%
  group_by(Round, Block, treatment, Day) %>%
  summarise(meanChl = mean(Chl, na.rm = T),
            Mesocosm = Mesocosm, Block = Block, 
            Day = Day, treatment = treatment,
            Round = Round,
            light = mean(light, na.rm = TRUE)) %>%
  ggplot(aes(y = meanChl, x = Day, group = interaction (treatment, Round))) +
    geom_line(aes(col = treatment)) +
    geom_point(aes(col = treatment)) +
    facet_grid(Round ~ Block)

plot_bymeso
```

There seems to be, to the eye, a clear block effect on algal densities and accrual, which is what we expected given different levels of light and shade.

It is hard to discern anything, so we fit a logistic growth model to estimate exponential growth rate $r$ and carrying capacity $K$. This is done using function `make_accrual`, which is called by `make_master`.

```{r, message=FALSE, warning=FALSE}
load('../data/data_master.Rda')
dataA <- subset(data, Round == 2 & Cichlid == 'N') %>%
  mutate(Treatment = droplevels(Treatment))#,
         #Males = relevel(Males, ref = 'ALP'),
         #Females = relevel(Females, ref = 'ALP'))
```


## Exponential growth rate (r) and carrying capacity (K)

First let's analyze by treatment, but not before plotting them.

**Exponential growth r**

```{r}
ggplot(dataA, aes(x = Treatment, y = chl_r)) +
  geom_boxplot()
```

**Carrying capacity K**

```{r}
ggplot(dataA, aes(x = Treatment, y = chl_K)) +
  geom_boxplot()
```

```{r}
model_r <- lm(chl_r ~ Block + cumlight + mean_flow + Treatment, dataA)
summary(model_r)

model_K <- lm(chl_K ~ Block + cumlight + mean_flow + Treatment, dataA)
kable(summary(model_K)$coef)
```

It seems like the carrying capacity K is higher in mesocosms with HP males and LP females. Could it be that harassed naive females, which normally eat more algae, are now eating less algae?

Let's do some planned comparisons.

```{r}
mat <- cbind(
  fish = c(-4, 1, 1, 1, 1),
  fem_pred = c(0, -1, 1, -1, 1),
  hp_on_lp = c(0, 0, 1, 0, -1),
  lp_on_hp = c(0, -1, 0, 1, 0)
)

contrasts(dataA$Treatment) <- mat

model_r <- lm(chl_r ~ Block + cumlight + mean_flow + Treatment, dataA)
kable(summary(model_r)$coef)

model_K <- lm(chl_K ~ Block + cumlight + mean_flow + Males * Females, dataA)
kable(summary(model_K)$coef)

```
Let's plot it.



```{r}
modelr <- lm(chl_r ~ Block * cumlight + mean_flow + Males * Females, dataA)

kable(summary(model_r)$coef)

modelK <- lm(chl_K ~ Block + cumlight + mean_flow + Males * Females, dataA)

kable(summary(model_K)$coef)

```


```{r}
emmK <- ggeffects::ggeffect(modelK, type = "emm", terms = c("Females", "Males"), se = TRUE)
emmr <- ggeffects::ggeffect(modelr, type = "emm", terms = c("Females", "Males"), se = TRUE)

dataAfish <- dataA %>% filter(Treatment != 'fishless')
dataAfishless <- dataA %>% filter(Treatment == 'fishless') %>%
  summarize(K0 = mean(chl_K),
            K_se = sd(chl_K)/n(),
            r0 = mean(chl_r),
            r_se = sd(chl_r)/n())

plotr <- ggplot(emmr, aes(x = x, y = predicted, group = group, shape = group)) +
  geom_hline(yintercept = dataAfishless$r0, col = "gray") +
  geom_hline(yintercept = c(dataAfishless$r0 + dataAfishless$r_se,
                            dataAfishless$r0 - dataAfishless$r_se), 
                            col = "gray", lty = 2) +
  geom_point(data = dataAfish, aes(x = Females, y = chl_r, group = Males),
             shape = 16, alpha = 0.3, size = 2.5, position = position_dodge(width = 0.75)) +
  geom_point(size = 3.5, position = position_dodge(width = 0.75)) +
  geom_errorbar(aes(ymin = predicted - std.error, ymax = predicted + std.error, group = group),
                position = position_dodge(width = 0.75), width = 0.1) +
  labs(x = 'Females', y = "Growth rate (r)",
       shape = 'Males') +
  scale_shape_manual(labels = c('Low Predation', 'High Predation'),
                     values = c(1, 17)) +
  theme_bw() +
   theme(panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        strip.background = element_blank(),
        legend.background = element_rect(linetype = 1),
        text = element_text(size = 15))

plotK <- ggplot(emmK, aes(x = x, y = predicted, group = group, shape = group)) +
  geom_hline(yintercept = dataAfishless$K0, col = "gray") +
  geom_hline(yintercept = c(dataAfishless$K0 + dataAfishless$K_se,
                            dataAfishless$K0 - dataAfishless$K_se), 
                            col = "gray", lty = 2) +
  geom_point(data = dataAfish, aes(x = Females, y = chl_K, group = Males),
             shape = 16, alpha = 0.3, size = 2.5, position = position_dodge(width = 0.75)) +
  geom_point(size = 3.5, position = position_dodge(width = 0.75)) +
  geom_errorbar(aes(ymin = predicted - std.error, ymax = predicted + std.error, group = group),
                position = position_dodge(width = 0.75), width = 0.1) +
  labs(x = 'Females', y = "Carrying Capacity (K)",
       shape = 'Males') +
  scale_shape_manual(labels = c('Low Predation', 'High Predation'),
                     values = c(1, 17)) +
  theme_bw() +
   theme(panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        strip.background = element_blank(),
        legend.position = 'right',
        legend.background = element_rect(linetype = 1),
        text = element_text(size = 15))

ggpubr::ggarrange(plotr, plotK, common.legend = TRUE, legend = 'top')
```

### Behaviors

**Effect of foraging**

```{r}
model_r <- lm(chl_r ~ Block + cumlight + mean_flow + Forage, dataA)

kable(summary(model_r)$coefficients)

model_K <- lm(chl_K ~ Block + cumlight + mean_flow + Forage, dataA)

kable(summary(model_K)$coefficients)
```

**Effect of sexual behaviors**

We add an interaction of the behaviors with female origin because we may expect the behavior to have a stronger effect in one population than the other (e.g. HP more 'resistant' to sneaking)

```{r}
model_r <- lm(chl_r ~ Block + cumlight + mean_flow + 
                Females * (Sneak + Sigmoid), dataA)

kable(summary(model_r)$coefficients)

model_K <- lm(chl_K ~ Block + cumlight + mean_flow + 
                Females * (Sneak + Sigmoid), dataA)

kable(summary(model_K)$coefficients)
```

Some trends that make sense but nothing significant. I guess pecking on algae is pretty low maintenance compared to inverts... let's check inverts.


### Chl at the end of the experiment

```{r}
load('../data/data_tiles.Rda')
data_tilesA <-data_tiles %>%
  filter(Round == 2 & cichlid == 'N' & Day == 28) %>%
  mutate(treatment = droplevels(factor(treatment)),
         treatment = relevel(treatment, ref = 'fishless'))

contrasts(data_tilesA$treatment) <- mat
```

Let's see how the average chl at the end of the experiment changes with treatment.

```{r}
ggplot(data_tilesA, aes(x = treatment, y = Chl)) +
  geom_boxplot()
```


```{r}
model <- lme(Chl ~ Block + cumlight + treatment, 
             random =~ 1| Mesocosm,
             data_tilesA)
summary(model)
```


