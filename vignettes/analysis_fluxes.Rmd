---
title: "Nutrient Fluxes"
author: "Andrés López-Sepulcre"
date: "2022-10-22"
output: html_document
---

```{r setup, include=FALSE}
library(knitr)
  opts_chunk$set(echo = TRUE)
library(mesocosm2021)
library(sjPlot)
load(paste0(here::here(),'/data/data_master.Rda'))
```

### Nitrogen

We first analyze the $NH_4$ fluxes:

```{r}
dataA = subset(data, Round == "2" & Cichlid == "N") %>%
  mutate(Treatment = droplevels(Treatment))

ggplot(dataA, aes(x = Treatment, y = N_flux)) +
  geom_boxplot()
```
```{r}
model <- lm(N_flux ~ Block + Treatment, dataA)

summary(model)
```
Much like in Bassar et al 2010. N flux is higher in HP treatments than LP treatments, which resemble more fishless channels.

How about our planned contrasts to see the effects of male-female coevolution?

```{r}
mat <- cbind(
  fish = c(-4, 1, 1, 1, 1),
  pred = c(0, -1, 0, 0, 1),
  hp_on_lp = c(0, 0, 1, 0, -1),
  lp_on_hp = c(0, -1, 0, 1, 0)
)

contrasts(dataA$Treatment) <- mat

model <- lm(N_flux ~ Block + Treatment, dataA)

kable(summary(model)$coef)
```

HP males have a negative effect on N-fluxes when on LP females.

```{r}
model <- lm(N_flux ~ Block + Females * Males, dataA)

kable(summary(model)$coefficients)
```

```{r}
emm <- ggeffects::ggeffect(model, type = "emm", terms = c("Females", "Males"), se = TRUE)

dataAfish <- dataA %>% filter(Treatment != 'fishless')
dataAfishless <- dataA %>% filter(Treatment == 'fishless') %>%
  summarize(Nflux0 = mean(N_flux),
            Nflux_se = sd(N_flux)/n())

plot_N <- ggplot(emm, aes(x = x, y = predicted, group = group, shape = group)) +
  geom_hline(yintercept = dataAfishless$Nflux0, col = "gray") +
  geom_hline(yintercept = c(dataAfishless$Nflux0 + dataAfishless$Nflux_se,
                            dataAfishless$Nflux0 - dataAfishless$Nflux_se), 
                            col = "gray", lty = 2) +
  geom_point(data = dataAfish, aes(x = Females, y = N_flux, group = Males),
             shape = 16, alpha = 0.3, size = 2.5, position = position_dodge(width = 0.75)) +
  geom_point(size = 3.5, position = position_dodge(width = 0.75)) +
  geom_errorbar(aes(ymin = predicted - std.error, ymax = predicted + std.error, group = group),
                position = position_dodge(width = 0.75), width = 0.1) +
  labs(x = 'Females', y = expression(paste(NH[4]," flux (",mu,"g/h)")),
       shape = 'Males') +
  scale_shape_manual(labels = c('Low Predation', 'High Predation'),
                     values = c(1, 17)) +
  theme_bw() +
   theme(panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        strip.background = element_blank(),
        legend.position = 'right',
        text = element_text(size = 15))
```



```{r}
model <- lm(N_flux ~ Block + Females * (Sneak + Sigmoid), dataA)

kable(summary(model)$coefficients)  
```

Nothing

### Phosphorus


We first analyze the $PO_3$ fluxes:

```{r}
ggplot(dataA, aes(x = Treatment, y = P_flux)) +
  geom_boxplot()
```
How about our planned contrasts to see the effects of male-female coevolution?

```{r}
model <- lm(P_flux ~ Block + Treatment, dataA)

kable(summary(model)$coef)
```

Some interaction with P fluxes being lowest when LP males are with HP females

```{r}
model <- lm(P_flux ~ Block + Females * Males, dataA)

kable(summary(model)$coefficients)
```

```{r}
emm <- ggeffects::ggeffect(model, type = "emm", terms = c("Females", "Males"), se = TRUE)

dataAfish <- dataA %>% filter(Treatment != 'fishless')
dataAfishless <- dataA %>% filter(Treatment == 'fishless') %>%
  summarize(Pflux0 = mean(P_flux),
            Pflux_se = sd(P_flux)/n())

plot_P <- ggplot(emm, aes(x = x, y = predicted, group = group, shape = group)) +
  geom_hline(yintercept = dataAfishless$Pflux0, col = "gray") +
  geom_hline(yintercept = dataAfishless$Pflux0 + dataAfishless$Pflux_se, col = "gray", lty = 2) +
  geom_hline(yintercept = dataAfishless$Pflux0 - dataAfishless$Pflux_se, col = "gray", lty = 2) +
  geom_point(data = dataAfish, aes(x = Females, y = N_flux, group = Males),
             shape = 16, alpha = 0.3, size = 2.5, position = position_dodge(width = 0.75)) +
  geom_point(size = 3.5, position = position_dodge(width = 0.75)) +
  geom_errorbar(aes(ymin = predicted - std.error, ymax = predicted + std.error, group = group),
                position = position_dodge(width = 0.75), width = 0.1) +
  labs(x = 'Females', y = expression(paste(PO[3]," flux (",mu,"g/h)")),
       shape = 'Males') +
  scale_shape_manual(labels = c('Low Predation', 'High Predation'),
                     values = c(1, 17)) +
  scale_x_discrete(labels=c("Low Predation", "High Predation")) +
  theme_bw() +
   theme(panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        strip.background = element_blank(),
        legend.position = 'right',
        text = element_text(size = 15))
```


```{r}
model <- lm(P_flux ~ Block + Females * (Sneak + Sigmoid), dataA)

kable(summary(model)$coefficients)  
```
```{r}
ggpubr::ggarrange(plot_N, plot_P, common.legend = TRUE, legend = 'top')
```

